# my_presentations

All my presentations in tex or pdf format. Since November 2018, I have completely gone over to abandon powerpoint presentations, and all  my presentations are in PDF or HTML (either beamer or Reveal.js). They are all in here. Free, open source.